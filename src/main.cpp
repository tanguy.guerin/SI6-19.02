#include <iostream>
#include <libpq-fe.h>
using namespace std;

int main(){
PGconn *connexion;
PGPing ping;
const char infos_connexion[] = ("host=postgresql.bts-malraux72.net port=5432 dbname=t.guerin connect_timeout=10 user=t.guerin");
connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=t.guerin connect_timeout=10 user=t.guerin");
ping=PQping(infos_connexion);
cout << "Test de la connexion au serveur Postgresql"<<endl;


//Requete question 4
const char requete[] =
"SELECT*FROM \"Animal\" INNER JOIN \"Race\" ON (\"Animal\".race_id = \"Race\".id) WHERE \"Animal\".sexe = 'F' and \"Race\".nom='Singuapura';";


if (ping==PQPING_OK)
{
cout << "Le serveur est accesible"<<endl;
connexion = PQconnectdb(infos_connexion);

if(PQstatus(connexion)==CONNECTION_OK)
{
cout<<"La connexion au serveur de base de données 'postgresql.bts-malraux72.net' a été étable avec les paramètres suivants :"<<endl;
cout<<"* utilisateur : "<<PQuser(connexion)<<endl;
cout<<"* mot de passe :";
for (int i = 0;i < sizeof(PQpass(connexion));i++){
cout<<"*";}
cout<<endl;
cout<<"* base de données :"<<PQdb(connexion)<<endl;
cout<<"* port TCP :"<<PQport(connexion)<<endl;


cout<<"* chiffrement SSL :";
bool ssl = PQsslInUse(connexion);
if ( ssl == 1){
cout<<"True"<<endl; }
else{
cout<<"False"<<endl;
}

cout<<"* encodage :"<<PQparameterStatus(connexion, "server_encoding")<<endl;
cout<<"* version du protocole :"<<PQprotocolVersion(connexion)<<endl;
cout<<"* version du serveur :"<<PQserverVersion(connexion)<<endl;
cout<<"* version de la bibliothèque 'libpq' du client :"<<PQlibVersion() <<endl;


cout<<"Connexion réussie !"<<endl;
}
else
{
cout<<"Echec de connexion !"<<endl;
}
}

if(ping == PQPING_REJECT)
{
cout<<"Pas de réponse"<<endl;
}


return 0;
}


