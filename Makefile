bin/main: obj/main.o
	g++ $(shell pkg-config --libs libpq) -o bin/main obj/main.o

obj/main.o: src/main.cpp
	g++ $(shell pkg-config --cflags libpq) -c -o obj/main.o src/main.cpp
clean:
	rm bin/* obj/*.o

repertoire:
	mkdir bin obj src
	touch bin/.gitkeep obj/.gitkeep
